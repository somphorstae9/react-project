import React from "react";

class From extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            form: {
                name: "",
                age: 0,
                gender: null,
                photo: null,
                programing: [],
                bDate: "",
                id: 0,
                majoring: null,
            },
            records: []
        }

    }

    setInputState = (key, event) =>
        this.setState((preState) => ({
            ...preState,
            form: {
                ...preState.form,
                [key]: event.target.value
            }
        }))


    onChangeCheckBox = (event) => {
        let finalPro = [...this.state.form.programing];

        if (event.target.checked) {
            finalPro.push(event.target.value);
        } else {
            finalPro = finalPro.filter(e => e != event.target.value)
        }

        this.setState((preState) => ({
            ...preState,
            form: {
                ...preState.form,
                programing: finalPro
            }
        }));

    }




    onSubmit = () => {
        this.setState((preState) => ({
            records: [...preState.records, this.state.form]
        }));
    }


    render() {
        const { records } = this.state;

        return <div className="p-5 row">
            <div className="col-12">
                <h2 className="text-center py-5">Register Form</h2>
                <div className="row">
                    <div className="col">
                        <label className="w-100"> Enter Name:
                        <input onChange={(event) => this.setInputState("name", event)}
                            className="form-control " type="text" />
                        </label><br />
                    </div>
                    <div className="col">
                        <label className="w-100">
                        ID
                        <input onChange={(event) => this.setInputState("id", event)} className="form-control" type="number" />
                        </label><br />
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <label className="w-100">
                        Age
                        <input onChange={(event) => this.setInputState("age", event)} className="form-control" type="number" />
                        </label><br />
                    </div>
                    <div className="col">
                    <label className="w-100">
                    BirthDay
                    <input onChange={(event) => this.setInputState("bDate", event)} className="form-control" type="date" />
                    </label><br />
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <label className="w-100">
                        Gender
                        <select onChange={(event) => this.setInputState("gender", event)} className="form-control">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            <option value="other">Other</option>
                        </select>
                        </label><br />
                    </div>
                    <div className="col">
                        <label className="w-100">
                        Photo
                        <input onChange={(event) => this.setInputState("photo", event)} className="form-control" type="file" />
                        </label><br />
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <label className="w-100">
                        Majoring
                        <select onChange={(event) => this.setInputState("majoring", event)} className="form-control">
                            <option value="Software Development">Software Development</option>
                            <option value="Computor Science">Computer Science</option>
                            <option value="other">Other</option>
                        </select>
                        </label><br />
                    </div>
                    <div className="col ">
                    <label className="p-2 w-100">
                    Programing
                    </label><br />
                    <label onChange={this.onChangeCheckBox} className="mx-2 ">
                        <input value="Js" className="mx-1" type="checkbox" />
                        Js
                    </label>

                    <label>
                        <input onChange={this.onChangeCheckBox} value="C++" className="mx-1" type="checkbox" />
                        C++
                    </label><br />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-8 offset-md-5">
                <input onClick={this.onSubmit} className="form-control mt-3 w-25 fw-bold bg-info" value="Submit" type="button" />
                    </div>
                </div>
            </div>

            <div className="row py-5 ">
            <div className="col12 ">
                <table className="table border ">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>ID</th>
                            <th>Age</th>
                            <th>Date of Birth</th>
                            <th>Gender</th>
                            <th>Photo</th>
                            <th>Majoring</th>
                            <th>Programing</th>
                        </tr>
                    </thead>
                    <tbody>
                        {records.length === 0 && <tr ><td className="text-center" colSpan={8}>
                            No Record</td></tr>}
                        {records.map((row, index) => {
                            return <tr key={index}>
                                <td>{row.name}</td>
                                <td>{row.id}</td>
                                <td>{row.age}</td>
                                <td>{row.bDate}</td>
                                <td>{row.gender}</td>
                                <td>
                                    <img src={row.photo} alt="__" />
                                </td>
                                <td>{row.majoring}</td>
                                <td>{row.programing}</td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    }


}

export default From;
