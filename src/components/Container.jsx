import React, { Component } from 'react'

export default class Container extends Component {
    constructor(props){
        super(props)
        this.state = {favoriteColor : "yellow"}
    }
  render() {
    return (
      <h1>{this.state.favoriteColor}</h1>
    );
  }
}
