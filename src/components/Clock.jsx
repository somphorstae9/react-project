
import React from 'react';

class Clock extends React.Component{
  constructor(props){
    super(props);
    this.state = {date: new Date  ()};
  }

  render(){
    return(
      <div>
        <h1>{this.props.title}</h1>
        <h2>It is {this.state.date?.toString()}</h2>
        <button onClick={() => this.setState({ date: new Date() })}>Get Date</button>
      </div>
    );
  }
}


export default  Clock ;
