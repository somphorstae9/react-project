import React from 'react'
import Car from './Car';

const Array1 = () => {
    const car = [
        {id: 1, brand: 'Ford'},
        {id: 2, brand: 'Bmw'},
        {id: 3, brand: "Toyota"}
    ];
  return (
    <div className='text-danger fw-bold text-uppercase'>
        {
          car.map((c) =>{
            return(
              <Car key={c.id} brand={c.brand}/>
            )
          })
        }
    </div>
  )
}

export default Array1